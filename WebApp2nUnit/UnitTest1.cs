using NUnit.Framework;
using webapp2;
using webapp2.ModelAccess;

namespace WebApp2nUnit
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }

        [Test]
        public void CheckDepartmentExist()
        {
            var obj = new Employee();

            var Res = obj.CheckDeptExist(10);

            Assert.That(Res, Is.True);
           
        }

        [Test]
        public void CheckDepartmentNotExist()
        {
            var obj = new Employee();

            var Res = obj.CheckDeptExist(10);

            Assert.That(Res, Is.True);
			 //Testing purpose
        }
    }
}